<span class='homepage_tabs'>
    <a href="archive" class='tab archive'>Archive</a>
    <span class='tab selected replies'>Replies</span>
    <a href="/" class='tab recent'>Recent</a>
</span>
<?php
    if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
        header ("Location: /");
    } else {
        echo "";
    }
    $favoritesql = "SELECT tweetid from favorites WHERE user = '$log_name'";
    $favoriteresult = mysqli_query($conn, $favoritesql);
    $favorites=array();
    $therearereplies = 0;
    if (!$favoriteresult) {
        printf("Timeline error: %s\n", mysqli_error($conn));
    }
    while($favoriterow = mysqli_fetch_assoc($favoriteresult)) {
        foreach ($favoriterow as $key=>$value) {
            if(!in_array($value, $favorites)){
                $favorites[]=$value;
            }
        }
    }
    $replysql = "SELECT * FROM tweets WHERE (tweet REGEXP '@$log_name' AND username NOT IN ('" . implode("','", $privateusers) . "') AND username NOT IN ('" . implode("','", $bannedusers) . "')) ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT 100";
    $replyget = mysqli_query($conn, $replysql);
    if (!$replyget) {
        printf("Timeline error: %s\n", mysqli_error($conn));
    }
    while($replyrow = mysqli_fetch_assoc($replyget)) {
        foreach ($replyrow as $key=>$value) {
            $favorite = "<span id='".$replyrow["id"]."' class='tweetbutton favorite'>[Favorite]</span>";
            if (strpos($value, $replyrow["tweet"]) === false) {
                continue;
            }
            if(in_array($replyrow["id"], $personalfavourings)){
                $favorite = "<span id='".$replyrow["id"]."' class='tweetbutton favorited'>[Unfavorite]</span>";
            }
            if($replyrow["username"] == $log_name) {
                $usercontent = "<span id='".$replyrow["id"]."' class='tweetbutton delete'>[Delete]</span>";
            } else {
                $usercontent = "";
            }
            $aquery = mysqli_query($conn, "SELECT * FROM `apps` WHERE `appname` = '".$tweetrow_single["sentfrom"]."'");
            $aresult = mysqli_fetch_assoc($aquery);
            if(mysqli_num_rows($aquery) == 1) {
                $sentfrom = "<a href='".$aresult['applink']."'>".$tweetrow_single["sentfrom"]."</a>";
            } else {
                $sentfrom = $tweetrow_single["sentfrom"];
            }
            echo("
                <div class='reply'>
                    <span class='reply_content'>
                        <b><a href='".$replyrow["username"]."'>".$replyrow["username"]."</a></b> ".$replyrow["tweet"]."
                        <a href='/statuses/".$replyrow["id"]."''><span class='timeago' title='".$replyrow["timestamp"]."".$globaluserinf['timezone']."'>".$replyrow["timestamp"]."</span></a> from web
                         $favorite $usercontent
                </div><br>");
            $therearereplies = 1;
            mysqli_query($conn, "UPDATE tweets SET tweet_read = 1 WHERE tweet REGEXP '@$log_name'");
        }
    }
    if ($therearereplies == 0) {
        echo "<em><b>You have no replies yet!</b> Interact with some users to get some! </em>";
    }
?>