<h2>Wayback Machine</h2>
<a target="a_blank" href="https://web.archive.org/web/20070202022702/http://www.twitter.com">Snapshot of the homepage from <em>February 2nd, 2007.</em></a><br>
<a target="a_blank" href="https://web.archive.org/web/20070206163143/http://twitter.com:80/biz"> Snapshot of Twitter co-founder's profile page from <em>February 6th, 2007</em></a><br>
<a target="a_blank" href="https://web.archive.org/web/20070210192030/http://twitter.com:80/biz/statuses/5314115"> Snapshot of a tweet from Twitter co-founder on <em>February 10th, 2007</em></a><br>
<a target="a_blank" href="https://web.archive.org/web/20070311235505/http://twitter.com:80/microsoft"> Snapshot of microsoft's page from <em>March 11th, 2007</em></a><br>
<a target="a_blank" href="http://web.archive.org/web/20070508225633/https://www.twitter.com/twitter"> Snapshot of twitter's page from <em>May 8th, 2007</em></a><br>
<br>
<h2>Images</h2>
<a target="a_blank" href="https://timedotcom.files.wordpress.com/2014/12/2007-twitter.jpg">Logged in screenshot of Twitter (date unknown, but presumed 2007 from layout)</a><br><br>
<h2>Videos</h2>
<a target="a_blank" href="https://www.youtube.com/watch?v=ddO9idmax0o">Twitter in Plain English</a> - includes screenshots from Twitter at the time, including one of the follow button.<br><br>
<h2>Articles</h2>
<a target="a_blank" href="https://www.bustle.com/articles/31726-happy-anniversary-twitter-heres-how-twitters-look-has-changed-from-2006-to-right-now-photos">Happy Anniversary, Twitter! Here's How Twitter's Look Has Changed, From 2006 To Right Now — PHOTOS</a>
<br><br>