<h1>Terms of Service</h1>
<p>The terms of service here are pretty generic, but you are still excepted to follow them.</p>
<p>Screech has the right to alter these at any time. You will be notified typically of changes to this TOS.</p><br>
<h2>General terms:</h2>
<ol>
    <li>You must be 13 years or older to use this site.</li>
    <li>You are responsible for anything that occurs under your account.</li>
    <li>You're responsible for keeping your password secure.</li>
</ol>
<h2>Usage terms:</h3>
<ol>
    <li><b>Do not intentionally harrass or bully others.</b> If it is clear based on your posts that you are trying to bring down an individual or account, you will be terminated.</b></li>
    <li><b>Do not post or link graphic content nudity.</b> <i>Screech is a strictly <b>SFW</b> site. If your post contains or links to graphic content, it will be removed.</i></li>
    <li><b>Do not violate others copyright unless it is protected under fair use.</b> This shouldn't be hard since most of what you link will be on other sites, but try to follow it anyway.</li>
    <li><b>Do not use Screech for illegal activies.</b> This includes child pornography, selling of prohibited drugs, piracy, and etc.</li>
    <li><b>Do not hack with malicious intent.</b></li>
    <li><b>Do not impersonate others with malicious intent.</b> You may make parody accounts, but they must say that they're a parody in their profile. If your impersonation account is created with malicious intent, like bullying someone, it will be terminated.</b></li>
    <li><b>Do not spam, either manually or from the API:</b> Sending a screech every second with the intent of flooding the timeline will result in a ban. <a href="http://web.archive.org/web/20190118210417/http://www.screech.xyz/public_timeline">Here's a very upfront example of what you shouldn't do.</a>
</ol>
<b>Failure to comply may result in tweet deletion or account suspension.</b><br><br>