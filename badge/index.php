<?php
include('../resources/connect.php');
$profile = $_GET['p'];
$bg = $_GET['bg'];
$limit = $_GET['limit'];
$show_replies_txt = "";
if(empty($limit)) {
    $limit = 5;
}
$show_replies = $_GET['show_replies'];
if($show_replies == 0) {
    $show_replies_txt = "AND tweet NOT RLIKE '(@[^\s]+)'";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $profile." on Screech!"?></title>
        <link rel="stylesheet" href="/badge/style.css" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="http://www.screech.xyz/resources/jquery.timeago.js"></script>
        <style>
        html {overflow-x: hidden; overflow-y: scroll;}
        </style>
    </head>
    <body>
        <span class='pf_hd'>
            <?php echo "<a target='a_blank' href='http://www.screech.xyz/".$profile."'>".$profile."</a> on <a target='a_blank' href='http://www.screech.xyz'>Screech</a>!";?>
        </span>
        <span class='pf_bd'>
            <?php
            $sql_pf = "SELECT * FROM users WHERE username = '$profile'";
            $result_pf = mysqli_query($conn, $sql_pf);
            if (!$result_pf) {
                printf("Error: %s\n", mysqli_error($conn));
            }
            $row_pf = mysqli_fetch_array($result_pf);
            $pf_bio = preg_replace("/(<(.*?)\>)/", "", $row_pf['bio'])
            ?>
            <span class='pf_inf'>
                <span class='pf_inf_img'>
                    <img src='../profiles/images/<?php echo $row_pf['username'];?>.png' width='64' height='64'>
                </span>
                <span class='pf_inf_o'>
                    <?php
                    if (!empty($row_pf['bio'])) {
                        echo "<strong>".$pf_bio."</strong><br><br>";
                    } else {
                        echo "<em>No bio set...</em><br><br>";
                    }
                    if (!empty($row_pf['fullname'])) {
                        echo "<em>Full name: ".$row_pf['fullname']."</em><br>";
                    }
                    if (!empty($row_pf['webpage'])) {
                        echo "<em>Web: ".$row_pf['webpage']."</em><br>";
                    }
                    if (!empty($row_pf['location'])) {
                        echo "<em>Location: ".$row_pf['location']."</em><br>";
                    }
                    ?>
                </span>
            </span><br>
            <span class='pf_psts'>
                <?php
                $sql_psts = "SELECT * FROM tweets WHERE username = '$profile' $show_replies_txt ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT $limit";
                $result_psts = mysqli_query($conn, $sql_psts);
                if (!$result_psts) {
                    printf("Error: %s\n", mysqli_error($conn));
                }
                while($row_psts = mysqli_fetch_assoc($result_psts)) {
                    foreach ($row_psts as $key=>$value) {
                        if (strpos($value, $row_psts["tweet"]) === false) {
                            continue;
                        }
                        echo "<span class='pf_pst'>\"".$row_psts['tweet']."\" <time class='timeago' datetime='".$row_psts["timestamp"]."+00:00'></time></span><br>";
                    }
                }
                ?>
            </span>
        </span>
     </body>
</html>