<!---

TO DO LIST (2.0):

* password reset function

* admin panel

* fix favorites page


--->
<?php
    $test = $_GET['test'];
    if ($test == 1) {
        error_reporting(E_ALL); ini_set('display_errors', 1); 
    } else {
        error_reporting(0);
    }
    $versionno = "0.9.02";
    $starttime = microtime(true);
    session_start();
    include("resources/connect.php");
    $globaluserinf = mysqli_fetch_array(mysqli_query($conn, "SELECT * FROM users WHERE username = '".$_SESSION["username"]."'"));
    $log_name = $globaluserinf['username'];
    $tweetcontents = $tweeterr = $back = $isStatus = $withFriends = $tweetid = $favorites_txt = $offlineonline = $defaultpage = "";
    $isProfile = $pageLoaded = $found = $allUpdates = $FavoritesPage = 0;
    $link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $linkFull = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $link = str_replace("http://","",$link);
    $link = str_replace("192.168.0.36","",$link);
    $link = str_replace("www.","",$link);
    $link = str_replace("screech.xyz","",$link);
    $link = str_replace("http://u952690596.hostingerapp.com/","",$link);
    $link = str_replace("/","",$link);
    $link = preg_replace("/\?(.*)/","",$link);
    $sql_user = "SELECT username FROM users WHERE username = $link.";
    $check = "pages/".$link.".php";
    $hideTweetBox = 'hideTweetBox';
    $tweet_query_single = "SELECT tweet, timestamp, username, id FROM tweets WHERE id = '$tweetid' ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT 9";
    $tweet_single_result = mysqli_query($conn, $tweet_query_single); 
    $tweetrow_single = mysqli_fetch_assoc($tweet_single_result);
    $sql_following = "SELECT * FROM following WHERE user1 = '$log_name'";
    $result_following = mysqli_query($conn, $sql_following);
    $row_following = mysqli_fetch_array($result_following);
    $allowphrase_txt = 'allowphrase';
    $justch = $_GET['justch'];
    
    // personal statistics
    $personalfollowers = $personalfavourings = array();
    $personaltweetsql = "SELECT * FROM tweets WHERE username = '$log_name'";
    $personaltweetresult = mysqli_query($conn, $personaltweetsql);
    $personaltweetno = mysqli_num_rows($personaltweetresult);
    $personalfavoritesql = "SELECT * from favorites WHERE user = '$log_name'";
    $personalfavoriteresult = mysqli_query($conn, $personalfavoritesql);
    $personalfavoriteno = mysqli_num_rows($personalfavoriteresult);
    $personalfollowersql = "SELECT * from following WHERE user2 = '$log_name'";
    $personalfollowerresult = mysqli_query($conn, $personalfollowersql);
    $personalfollowerno = mysqli_num_rows($personalfollowerresult);
    $personalfollowingsql = "SELECT * from following WHERE user1 = '$log_name'";
    $personalfollowingresult = mysqli_query($conn, $personalfollowingsql);
    $personalfollowingno = mysqli_num_rows($personalfollowingresult);
    $personalfavoritesql = "SELECT * from favorites WHERE user = '$log_name'";
    $personalfavoriteresult = mysqli_query($conn, $personalfavoritesql);
    $personalfavoriteno = mysqli_num_rows($personalfavoriteresult);
    $pmsql = "SELECT * from messages WHERE user2 = '$log_name'";
    $pmresult = mysqli_query($conn, $pmsql);
    $pmno = mysqli_num_rows($pmresult);
    
    while($personalfollowerrow = mysqli_fetch_assoc($personalfollowerresult)) {   
        foreach ($personalfollowerrow as $key=>$value) {
            if (strpos($value, $personalfollowerrow["user1"]) === false) {
                continue;
            }
            array_push($personalfollowers, $personalfollowerrow['user1']);
        }
    }
    while($personalfavoriterow = mysqli_fetch_assoc($personalfavoriteresult)) {   
        foreach ($personalfavoriterow as $key=>$value) {
            if (strpos($value, $personalfavoriterow["tweetid"]) === false) {
                continue;
            }
            array_push($personalfavourings, $personalfavoriterow["tweetid"]);
        }
    }
    if ($justch == 1) {
        echo "<script>window.alert('\\(If you do not see a change, clear your browser\'s cache and then reload.\\)')</script>";
    }
    if (file_exists($check)) {
        $destination = $check;
        $pageLoaded = 1;
        $isProfile = 0;
    }
    if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
        $offlineonline = "offline";
    } else {
        $offlineonline = "online";
    }
    if(empty($link)) {
        if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
            $destination = "resources/offline.php";
            $isProfile = 0;
            $hideTweetBox = '';
            $pageLoaded = 1;
            $defaultpage = 'defaultpage';
        } else {
            $destination = "resources/online.php";
            $isProfile = 0;
            $hideTweetBox = '';
            $pageLoaded = 1;
            $defaultpage = 'defaultpage';
        }
    }
    if (strpos($link, 'with_friends') !== false) {
        $link = str_replace("with_friends","",$link);
        $isProfile = 1;
        $pageLoaded = 1;
        $withFriends = "withFriends";
    }
    if (strpos($linkFull, '/all_updates') !== false || strpos($linkFull, 'all_updates/') !== false || strpos($linkFull, '/all_updates/') !== false) {
        $link = str_replace("all_updates","",$link);
        $isProfile = 1;
        $pageLoaded = 1;
        $allUpdates = 1;
    }
    if (strpos($linkFull, '/favorites') !== false || strpos($linkFull, 'favorites/') !== false || strpos($linkFull, '/favorites/') !== false) {
        $link = str_replace("favorites","",$link);
        $isProfile = 1;
        $pageLoaded = 1;
        $favorites_txt = 'favorites_page';
    }
    if (strpos($linkFull, '/profile_image') !== false) {
        if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
            header("Location: /login");
        }
        $link = str_replace("profile_image","",$link);
        echo "<style>body {background: #222;}</style><center><img src='/profiles/images/$link.png' style='position: absolute; top: 50%; left: 50%; transform: translate(-50%,-50%);'></center>";
        die;
    }
    $prof_user = mysqli_real_escape_string($conn, $link);
    $usercheck = mysqli_query($conn, "SELECT username from users where username = '$prof_user' LIMIT 1");
    if (mysqli_fetch_row($usercheck) && $pageLoaded == 0) {
        $prof_user = $link;
        $isProfile = 1;
        $pageLoaded = 1;
        $hideTweetBox = 'hideTweetBox';
    } else {
        echo("");
    }
    if ((strpos($link, 'settings') !== false) || (strpos($link, 'login') !== false) || (strpos($link, 'statuses') !== false) || (strpos($link, 'invite') !== false) || (strpos($link, 'references') !== false) || (strpos($link, 'help') !== false)) {
        $allowphrase_txt = "";
    }
    if (strpos($link, 'statuses') !== false) {
        $destination = "resources/status.php";
        $isProfile = 0;
        $pageLoaded = 1;
        $isStatus = "isStatus";
        $link = str_replace("statuses","",$link);
    }
    if ((strpos($link, 'archive') !== false) || (strpos($link, 'replies') !== false)) {
        $hideTweetBox = '';
        $defaultpage = 'defaultpage';
    }
    if ($pageLoaded == 0) {
        $destination = "pages/404.php";
    }
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['tweet_post'])) {
               $tweetcontents = $_POST["tweet-box-text"];
                if (empty($tweetcontents)) {
                    $tweeterr = "We need something here.";
                }
                if ((strlen($tweetcontents - 2)) > 140) {
                    $tweeterr = "Your screech is too long. Try again.";
                }
                if (empty($log_name)) {
                    $tweeterr = "Your session name is empty. Please log out then log back and try again.";
                }
                if (empty($tweeterr)) {
                    $sql = "INSERT INTO tweets (username, tweet, sentfrom) VALUES (?, ?, ?)";
                    $okay = "web";
                    if($stmt = mysqli_prepare($conn, $sql)){
                        mysqli_stmt_bind_param($stmt, "sss", $log_name, htmlspecialchars($tweetcontents), $okay);
                        if(mysqli_stmt_execute($stmt)){
                            header("Location: /");
                        } else{
                            echo "Something went wrong. Please try again later.";
                        }
                    }
                }
            }
    }
    
    if($isProfile==0){echo "<div class='snowflakes'><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i><i></i></div>
    ";}
?>

<script>
    var log_name = "<?php echo $log_name?>";
    var prof_user = "<?php echo $prof_user?>";
</script>
<html prefix="og: http://ogp.me/ns#">
    <head>
        <?php
        if ($isStatus !== "isStatus") {
            echo "<title>Screech";
            if ($isProfile == 1) {
                echo " / ".$prof_user;
            } else {
                if (empty($link)) {
                    if($offlineonline == "online") {
                        echo ": home";
                    } else if($offlineonline == "online") {
                        echo ": What are you doing?";
                    }
                } else{
                    echo ": ".ucfirst($link);
                }
            }
            echo "</title>";
        }
        ?>
        <link rel="stylesheet" type="text/css" href="/resources/style.css">
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src='/resources/jquery.js'></script>
        <script src='/resources/jquery.timeago.js'></script>
        <script src='/resources/jscolor.js'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/showdown/1.8.7/showdown.js'></script>
        <script src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    </head>
    <body class="<?php if (!empty($link)) {echo "page_".$link." ";} else {echo "page_home ";}; echo $defaultpage." ".$favorites_txt." ".$isStatus." ".$offlineonline." ".$hideTweetBox." "; if($isProfile == 1) {echo "isProfile";};?>">
        <span class="container_wrap">
        <div class="container">
            <h1 class="header">
                <a class='img_logo' href="/">
                    <img src="/resources/logo.png">
                    <span class='img_stitle no-select'>BETA</span>
                </a>
            <?php 
            if (isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == true) {
                echo "<span class='top_nav'>
                    <a href='/'>Home</a> |
                    <a href='/$log_name'>Your profile</a> |
                    <a href='/invite'>Invite</a> |
                    <a href='/public_timeline'>Public Timeline</a> |
                    <a href='/badges'>Badges</a> |
                    <a href='/settings'>Settings</a> |
                    <a href='/logout'>Sign out</a>
                </span>";
            }
            ?>
            </h1>
            <div class='content <?php echo $allowphrase_txt." "?>'>
            <div class="send-tweet">
                <?php
                    if (isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == true) {
                        echo "
                        <form action='' method='post' class='tweet-box-wrapper'>
                            <div class='tweet-box'>
                            <h2 class='tweet-box-head'>What are you doing?<span class='tweet-box-chars'>140</span></h2>
                            <textarea rows='4' name='tweet-box-text' class='tweet-box-text'>$tweetcontents</textarea>
                            <span class='tweet_err error'>$tweeterr</span>
                            </div>
                            <input type='submit' value='Update' name='tweet_post' class='btn submit'>
                        </form>";
                    }
                ?>
            </div>
            <div class="profile-info">
                <?php 
                    if($isProfile == 1) {
                        if ($FavoritesPage = 0) {
                        error_reporting(0);
                        echo "<h2 class='prof_name'>
                                <a>
                                    <div class='image_wrapper'>
                                        <img width='100%' height='100%' src='/profiles/images/".$prof_user.".png'>
                                    </div>
                                </a>".$prof_user."</h2>";
                        error_reporting(1);
                        }
                        include('resources/profile.php');
                    }
                ?>
            </div>
                <?php 
                    if ($isProfile == 0) {
                        echo ("<div class='wrapper'>");
                        include($destination);
                        
                        echo ("</div>");
                    }
                ?>
                <?php
                    if ($isStatus == "isStatus") {
                        echo "<span class='status_info'>
                            <span class='arrow2_status'></span>
                                <h2 class='prof_name'>
                                    <a>
                                        <div class='image_wrapper'>
                                            <img width='100%' height='100%' src='/profiles/images/".$tweetrow_single["username"].".png'>
                                        </div>
                                    </a> <a href='../".$tweetrow_single["username"]."'>".$tweetrow_single["username"]."</a>
                                </h2>
                          </span>";
                    }
                    ?>
            </div>
            <div class='sidebar_wrap'>
            <div class="sidebar">
            <?php
            if((!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) && $isProfile == 0){
                $logouttext = "";
                error_reporting(1);
                include("resources/sidebar_offline.php");
            } else {
                if(isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] == true) {
                    $logouttext = "";
                } else {
                    $logouttext = "";
                    $offlinediv = "<div class='signup'>Want an account?<a href='signup'>Join for Free!</a>It's fast and easy!</div>";
                }
                error_reporting(1);
                include("resources/sidebar_online.php");
            }?>
            </div>
                <div class='sim_links'>
                    <b class='sim_header'>SIMILAR/AFFILIATE PROJECTS</b>
                    <a class='sim_a' href='https://www.vidlii.com'>
                        <span class='sim_img vid'></span> Vidlii
                    </a>
                    <a class='sim_a' href='https://www.friendproject.net'>
                        <span class='sim_img fp'></span> Friend Project
                    </a>
                    <a class='sim_a' href='http://iwarg.ddns.net/aim/index.php?action=main'>
                        <span class='sim_img aim'></span> AIM Phoenix
                    </a>
                    <a class='sim_a' href='https://ipgflip.xyz'>
                        <span class='sim_img sudo'></span> IPGFlip
                    </a>
                </div>
            <span class='search_bar'>
                <form action='search'><p>
                    <input type='text' name='q'>
                    <input type='submit' value='Search'>
                </p></form>
            </span>
            </div>
            <div class="footer">
            © 2018 IoI_xD | <a href="/about">About</a> | <a href="/contact">Contact</a> | <a href="/screech">@screech</a> | <a href="/help">Help</a> | <a href="/tos">Terms of Service</a> | <a href="/reference">References Used</a> |
<form style='display: inline-block; vertical-align: middle;' action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank">
<input type="hidden" name="cmd" value="_s-xclick" />
<input type="hidden" name="hosted_button_id" value="W7VWA3E67FB4E" />
<input type="image" src="https://www.paypal.com/en_US/i/btn/x-click-but04.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1" />
</form>
            </div>
        </div>
        </span>
<?
    $endtime = microtime(true);
    echo("<span class='loadtime'>");
    printf("Page loaded in %f seconds", $endtime - $starttime );
    echo (" - Screech version $versionno");
    echo("</span>");
?>
    <style></style>
    </body>
</html>