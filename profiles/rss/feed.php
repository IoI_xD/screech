<?php
	include("/var/www/screech/resources/connect.php");
	header("Content-Type: application/rss+xml; charset=ISO-8859-1");
	$userinfq = mysqli_query($conn, "SELECT * FROM `users` WHERE username = '".$_GET['user']."'");
	$userinf = mysqli_fetch_array($userinfq);
    if($userinf['private'] == 1) {
        die("RSS Feeds from private accounts are not publicly avaliable");
    }
	$tweetinf = mysqli_query($conn, "SELECT * FROM `tweets` WHERE username = '".$_GET['user']."' ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT 40");
	if(!$tweetinf) {
		echo mysqli_error($conn)."SELECT * FROM `tweets` WHERE username = '".$_GET['user']."' ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT 40";
	}
	echo "<?xml version='1.0' encoding='UTF-8' ?>
<rss version='2.0'>
	<channel>
		<title>Screech / ".$userinf['fullname']."</title>
		<link>http://screech.xyz/".$userinf['username']."</link>
		<description>Screech updates from ".$userinf['username']."</description>
		<language>en-us</language>
		<ttl>40</ttl>
";
    while($screechrow = mysqli_fetch_assoc($tweetinf)) {   
        foreach ($screechrow as $key=>$value) {
            if (strpos($value, $screechrow["tweet"]) === false) {
                continue;
            }
			echo "		<item>
			<title>".$userinf['username'].": ".$screechrow['tweet']."</title>
			<description>".$userinf['username'].": ".$screechrow['tweet']."</description>
			<pubdate>".$screechrow['timestamp']."</pubdate>
			<guid>http://screech.xyz/statuses/".$screechrow['id']."</guid>
			<link>http://screech.xyz/statuses/".$screechrow['id']."</link>
		</item>
";
        }
    }
	echo "	</channel>
</rss>";